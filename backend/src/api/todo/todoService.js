//Recupera o model do Todo do module.exports
const Todo = require('./todo');

//Métodos habilitados para o model Todo
Todo.methods(['get','post','put','delete']);

//Quando atualizar um determinado registro, enviar como resposta o documento|registro atualizado
Todo.updateOptions({new: true, runValitadors: true});

module.exports = Todo;

