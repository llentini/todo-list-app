import React from 'react'
import IconButton from '../template/iconButton'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { deleteTodo, updateTodo } from './todoActions'

const TodoList = props => {
    
    const renderRows = () => {
        const list = props.list || []

        return list.map(todo => (
            <tr key={todo._id}>
                <td className={todo.done ? 'markedAsDone' : ''}>{todo.description}</td>
                <td>
                    <IconButton style='success' icon='check' show={!todo.done} onClick={() => props.updateTodo(todo)}></IconButton>
                    <IconButton style='warning' icon='undo' show={todo.done} onClick={() => props.updateTodo(todo)}></IconButton>
                    <IconButton style='danger' icon='trash-o' show={true} onClick={() => props.deleteTodo(todo)}></IconButton>
                </td>
            </tr>
        ))
    }

    return (
        <section className="sectionInfo">
            <table className='table'>
                <thead>
                    <tr>
                        <th>Descrição</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {renderRows()}
                </tbody>
            </table>
        </section>
    )
}

const mapStateToProps = state => ({ list: state.todo.list })
const mapDispatchToProps = dispatch => bindActionCreators({deleteTodo, updateTodo}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)