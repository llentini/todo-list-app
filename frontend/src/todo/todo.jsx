import React, {Component} from 'react'
import axios from 'axios'

//Components
import Header from '../template/header';
import TodoForm from './todoForm';
import TodoList from './todoList';

import { changePageInfo } from './todoActions';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


class Todo extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.changePageInfo("Tarefas","Cadastro")
    }

    render() {
        return (
            <div>
                <Header></Header>
                <TodoForm ></TodoForm>
                <TodoList></TodoList>
            </div>
        )
    }
}

const mapStateToProps = state => ({ state })
const mapDispatchToProps = dispatch => bindActionCreators({ changePageInfo }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Todo)