import * as constants from '../helpers/constants';

const INITIAL_STATE = {
    description: "",
    list: [],
    name: "Teste",
    small: "Page"
};

export default (state = INITIAL_STATE, action) => {
    console.log("todoReducer.js",state,action);

    switch(action.type) {
        case constants.DESCRIPTION_CHANGED:
            return { ...state, description: action.payload };
        case constants.LIST_CHANGED:
            return { ...state, list: action.payload};
        case constants.TODO_CLEAR:
            return { ...state, description: ""};
        case constants.TODO_UPDATED:
            /*let updated_list = state.list
            let index = updated_list.findIndex(x => x._id === action.payload._id);
            updated_list[1].done = action.payload.done
            return { ...state, list: updated_list };*/
            return state;
        case constants.PAGE_INFO:
            return { ...state, name: action.payload.name, small: action.payload.small }
        default:
            return state;
    }
}