import React, { Component } from 'react';

import Grid from '../template/grid';
import IconButton from '../template/iconButton';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeDescription, search, createTodo } from './todoActions';


class TodoForm extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        console.log("WillMount()");

        //Método search do todoActions.js
        this.props.search()
    }
    
    render() {
        const { description, createTodo } = this.props; 

        return (
            <div role="form" className="todoForm">
                <Grid cols="12 9 10">
                    <input id="description" className="form-control" placeholder="Descrição da Tarefa" 
                    onChange={this.props.changeDescription}
                    value={description}>
                    </input>
                </Grid>
                
                <Grid cols="12 3 2">
                    <IconButton style="primary" icon="plus" show={true}
                        onClick={() => createTodo(description)}
                    ></IconButton>
                    {/*<IconButton style="info" icon="search" show={true}
                        onClick={() => search()}
                    ></IconButton>*/}
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = state => ({ description: state.todo.description })
const mapDispatchToProps = dispatch => bindActionCreators({changeDescription, search, createTodo}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TodoForm)