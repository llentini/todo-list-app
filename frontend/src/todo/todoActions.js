import axios from 'axios';
import * as constants from '../helpers/constants';

export const changeDescription = event => {
    console.log("changeDescription",event.target.value);
    return [{
        type: constants.DESCRIPTION_CHANGED,
        payload: event.target.value
    }, search(event.target.value)]
};

export const search = () => {
    console.log("search");
    return (dispatch, getState) => {
        const description = getState().todo.description
        const search = description ? "&description__regex="+description : "";
        const request = axios.get(constants.URL+'?sort=-createdAt'+search)
            .then(resp => dispatch({
                                type: constants.LIST_CHANGED,
                                payload: resp.data
                                })
                )
    }
};

export const clear = () => {
    console.log("clear");
    return {
        type: constants.TODO_CLEAR
    }
}

/*export const createTodo = description => {
    console.log("createTodo", description);
    const request = axios.post(constants.URL, { description });

    return [
        {type: 'TODO_CREATED', payload: request},
        search()
    ]
};*/

export const createTodo = description => {
    console.log("createTodo", description);

    /*
    * Primeiro then para disparar a ação para limpar o campo de busca/adicionar
    * Segundo then para chamar o método search e disparar o método para atualizar a lista, após criar o objeto no banco
    */
    return dispatch => {
        axios.post(constants.URL, { description })
            .then(resp => dispatch(clear()))
            .then(resp => dispatch(search()))
    }
};

export const deleteTodo = todo => {
    console.log("deleteTodo", todo);
    /*
    * Primeiro then para disparar a ação que deletou um objeto
    * Segundo then para chamar o método search e disparar o método para atualizar a lista, após deletar o objeto no banco
    */
    return dispatch => {
        axios.delete(constants.URL+''+todo._id)
            .then(resp => dispatch({ type: constants.TODO_DELETED }))
            .then(resp => dispatch(search()))
    }
};

export const updateTodo = todo => {
    console.log("updateTodo", todo.done);
    /*
    * Primeiro then para disparar a ação que atualizou (marcou como concluido ou não) um objeto
    * Segundo then para chamar o método search e disparar o método para atualizar a lista, após atualizar o objeto no banco
    */
    return dispatch => {
        axios.put(constants.URL+''+todo._id, { ...todo, done: !todo.done})
            .then(resp => dispatch({ type: constants.TODO_UPDATED, payload: resp.data }))
            .then(resp => dispatch(search()))
    }
};


export const changePageInfo = (name, small) => {
    console.log("changePageInfo", name, small);
    return {
        type: constants.PAGE_INFO,
        payload: {name, small}
    }
}