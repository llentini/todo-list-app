import 'modules/bootstrap/dist/css/bootstrap.min.css'
import 'modules/font-awesome/css/font-awesome.min.css'
import '../template/custom.css'
import React from 'react'

//Routes
import Routes from './routes';

//Components
import Menu from '../template/menu';

export default props => (
    <div>
        <Menu />
        <Routes />
    </div>
)

