import React, {Component} from 'react'
import Header from '../template/header';

import { connect } from 'react-redux';
import { changePageInfo } from '../todo/todoActions';
import { bindActionCreators } from 'redux';


class About extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.changePageInfo("About","Us");
    }

    render() {
        return (
            <div>
                <Header></Header>

                <h2>Pages using Routes</h2>
                <p>Lorem ipsum dolor sit amet...</p>
            </div>
        )
    }
}

const mapStateToProps = state => ({ state })
const mapDispatchToProps = dispatch => bindActionCreators({ changePageInfo }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(About)