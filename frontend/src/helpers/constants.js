export const DESCRIPTION_CHANGED = 'DESCRIPTION_CHANGED';
export const LIST_CHANGED = 'LIST_CHANGED';
export const TODO_CREATED = 'TODO_CREATED';
export const TODO_DELETED = 'TODO_DELETED';
export const TODO_UPDATED = 'TODO_UPDATED';
export const TODO_CLEAR = 'TODO_CLEAR';
export const URL = 'http://localhost:3003/api/todos/';
export const PAGE_INFO = 'PAGE_INFO';