import React from 'react';

import { connect } from 'react-redux';

const Header = props => (
    <header className="page-header">
        <h2>{props.name} <small>{props.small}</small></h2>
    </header>
)

const mapStateToProps = state => ({name: state.todo.name, small: state.todo.small});

export default connect(mapStateToProps)(Header)
